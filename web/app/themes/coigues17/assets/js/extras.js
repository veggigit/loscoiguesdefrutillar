// ID -> agregamos un id a los items
function newid(){
  $("#galeria .item").each(function(i) {
    $(this).attr('id', 'imgid_0' + i);
  });
}
// MODAL CSS -> estilos full page para modal
function modalcss(){
  $(".modal-fullscreen").on('show.bs.modal', function () {
    setTimeout( function() {
      $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
    }, 0);
  });
  $(".modal-fullscreen").on('hidden.bs.modal', function () {
    $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
  });
}
// CALL MODAL -> llamamos al modal plano
function modalplano(){
  $('#callplano').click(function(){
    $('#ModalFull .modal-body').empty(); // Muy importante: Hay que mencionar el modal #ModalFull, con esto evitamos que esta accion se repita en el modal carousel
    $($(this).children('.img-responsive').clone().appendTo('#ModalFull .modal-body')); // Muy importante: Hay que mencionar el modal #ModalFull, con esto evitamos que esta accion se repita en el modal carousel
    $('#ModalFull').modal('show');
  });
}
// GALERY -> convertimos al modal en una galeria con carousel, ver moddal.php
function modalgallery() {
  $('.carousel').carousel({interval: false});
  $("#galeria .item").each(function() {
    $(this).clone().appendTo("#Modalgallery #Mycarousel .carousel-inner");
  });
  var Gitem = $("#galeria .item");
  var Citem = $("#Mycarousel .item");
  var Cinner = $("#Mycarousel .carousel-inner");

  $(Gitem).click(function(){
    $("#Mycarousel .active").removeClass("active");
    var gid = this.id;
    var active = function (){
      Cinner.find('#' + gid).addClass("active");
    }
    active();
    $("#Modalgallery").modal("show");
  });
}
// PLUGIN -> iniciamos plugin para copiado.
function clipboard() {
  // Tooltip mensaje bootstrap  "Copiado"
  // http://stackoverflow.com/questions/37381640/tooltips-highlight-animation-with-clipboard-js-click/37395225
  $('.clip').tooltip({
    trigger: 'click',
    placement: 'bottom'
  });
  function setTooltip(btn, message) {
    $('.clip').tooltip('hide')
    .attr('data-original-title', message)
    .tooltip('show');
  }
  function hideTooltip(btn) {
    setTimeout(function() {
      $('.clip').tooltip('hide');
    }, 3000);
  }
  // Clipboard
  var clipboard = new Clipboard('.clip');
  clipboard.on('success', function(e) {
    setTooltip(e.trigger, 'Copiado!');
    hideTooltip(e.trigger);
  });
  clipboard.on('error', function(e) {
    setTooltip(e.trigger, 'Fail!');
    hideTooltip(e.trigger);
  });
}

// LAUNCHER
$(document).ready(function(){
  // refresh();
  newid ();
  modalcss();
  modalplano();
  modalgallery();
  clipboard();
});
