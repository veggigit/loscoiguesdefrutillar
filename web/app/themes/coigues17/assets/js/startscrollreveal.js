// setup e Iniciamos el scrollreveal.js, IMPORTANTE, debe ir al final de los elementos involucrados :D
// var fooReveal = {
//   delay    : 200,
//   distance : '90px',
//   easing   : 'ease-in-out',
//   rotate   : { z: 10 },
//   scale    : 1.1
// };

window.sr = ScrollReveal();
sr.reveal('.scroll-motion', { delay: 200, scale: 0.9 });
