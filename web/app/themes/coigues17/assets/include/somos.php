<?php
// ************************************************
// **** SOMOS TEMPLATE ****************************
// ************************************************
 ?>
 <section id="somos" class="full-page">
   <div class="container">
     <div class="row scroll-motion">
       <?php page_content(2); ?>
     </div>
   </div>
 </section>
