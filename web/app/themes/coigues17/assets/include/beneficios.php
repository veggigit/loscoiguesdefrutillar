<?php
// ************************************************
// **** BENEFICIOS TEMPLATE ***********************
// ************************************************
 ?>
 <section id="beneficios" class="full-page">
   <div class="container">
     <div class="row scroll-motion">
       <?php page_content(4); ?>
     </div>
   </div>
 </section>
