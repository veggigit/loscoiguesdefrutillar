<?php
// ************************************************
// **** CONTACTO TEMPLATE *************************
// ************************************************
 ?>
 <section id="contacto">
   <div class="email">
     <div class="container">
       <div class="row text-center">
         <div class="titulo">
           <h2>Contáctanos en:</h2>
         </div>
         <span>contacto@loscoiguesdefrutillar.cl</span>
         <button type="button" class="btn btn-info clip" data-clipboard-text="contacto@loscoiguesdefrutillar.cl">Copy email</button>
       </div>
     </div>
   </div>
   <div class="geo">
     <div class="container">
       <div class="row scroll-motion">
         <img class="img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/img/ubicacion.jpg">
       </div>
     </div>
   </div>
 </section>
