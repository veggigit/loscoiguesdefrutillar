<?php
// ************************************************
// **** GALERY TEMPLATE ***************************
// ************************************************
 ?>
 <section id="galeria">
   <div class="container-fluid">
     <div class="row no-gutter">
       <?php galeria(16); ?>
     </div>
   </div>
 </section>
