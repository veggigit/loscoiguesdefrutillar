<?php
 ?>

<div class="modal modal-fullscreen fade" id="Modalgallery" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button class="close" type="button" data-dismiss="modal">×</button>
          <h3 class="modal-title">El sector</h3>
      </div>
      <div class="modal-body">
          <!-- star carousel -->
          <div id="Mycarousel" class="carousel slide"  data-ride="carousel">
            <div class="carousel-inner">
            </div>
            <a class="carousel-control left" href="#Mycarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
            <a class="carousel-control right" href="#Mycarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
          </div>
          <!-- end carousel -->
      </div>
      <div class="modal-footer">
          <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



 <!-- SIMPLE MODAL -->
 <div class="modal modal-fullscreen fade" id="ModalFull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
         <h3 class="modal-title" id="myModalLabel">Parcelación</h3>
       </div>
       <div class="modal-body">

       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
       </div>
     </div>
   </div>
 </div>
