<?php
// ************************************************
// **** FOOTER CONTENT ****************************
// ************************************************
?>
<footer id="datos">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-brand">
        <div class="brand-final">
          <img src="<?php bloginfo('template_directory'); ?>/img/brand-footer.jpg">
        </div>
      </div>
      <div class="col-md-6 col-datos">
        <address>
          <strong>Parcelación Los Coigües de Frutillar</strong></br>
          Frutillar, camino a Puerto Octay</br>
          <abbr title="Phone">Whatsapp/Fono:</abbr> +569 72163515</br>
          <a href="mailto:contacto@loscoiguesdefrutillar.cl">contacto@loscoiguesdefrutillar.cl</a>
        </address>
      </div>
    </div>
  </div>
</footer>
<!-- este script debe estar despues del footer  -->
<script src="<?php bloginfo('template_directory'); ?>/assets/js/myanchor.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/startscrollreveal.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/extras.js"></script>
</body>
</html>
