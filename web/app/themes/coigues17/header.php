<?php
// ************************************************
// **** HEADER CONTENT ****************************
// ************************************************
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="UTF-8">
  <title><?php bloginfo ('name'); ?></title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta name="description" content="Ventas parcelas los Coigues">
  <meta name="Keywords" content="venta parcelas">
  <!-- load jquery -->
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery3.1.1.min.js"></script>
  <!-- load bootstrap core -->
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css">
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
  <!-- load google fonts & icons -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
  <!-- load css -->
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/main.css">
  <!-- load extras js -->
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/clipboard.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/scrollreveal.min.js"></script>

</head>
<body>
  <!-- nav -->
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">
          <img src="<?php bloginfo('template_directory'); ?>/img/brand-coigues.png" alt="<?php bloginfo ('name'); ?>">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a class="underline" href="#somos">Somos</a></li>
          <li><a class="underline" href="#beneficios">Beneficios</a></li>
          <li><a class="underline" href="#galeria">Galería</a></li>
          <li><a class="underline" href="#mapa">Master plan</a></li>
          <li><a class="underline" href="#datos">Dónde estamos</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- header -->
  <header id="home" class="full-page">
    <div class="container">
      <div class="row flex">
        <?php page_content(74); ?>
      </div>
    </div>
  </header>
