<?php
//**********************************************************************************************
// **** GET PAGE CONTENT ***********************************************************************
//**********************************************************************************************
?>
<?php
// Creamos funcion page_content ($post_id/variable), en el parentesis se capta el id del post, en este caso post id = page id.
function page_content ($post_id){
  // Luego creamos la variable $post, esta capta el post_id.
  $post = get_post($post_id);
  // Creamos la variable $title_page. Por medio de get_post_field captamos el titulo (post_title wp) del id $post. Variable anteriormente definida.
  $title_page = get_post_field( 'post_title', $post ); //https://codex.wordpress.org/Function_Reference/get_post_field
  // Creamos la variable $content_page. Por medio de get_post_field captamos el contenido (post_content wp) del id $post. Variable anteriormente definida.
  $content_page = get_post_field( 'post_content', $post ); //https://codex.wordpress.org/Function_Reference/get_post_field
  // Creamos variable de ruta
  $blog_link =  get_bloginfo( 'template_directory' );
  // Condicionamos e Imprimimos
  if ($post_id == 74){
    echo "<div class='col-md-4'><img src=$blog_link/img/brand-coigues.png></div><div class='col-md-8'><div class='titulo text-center'><h1>$content_page</h1></div></div>";
  } else {
    echo "<div class='titulo'><h2>$title_page</h2></div>","<div class='contenido'><p>$content_page</p></div>";
  }
}
?>

<?php
//**********************************************************************************************
// **** GET GALERY PAGE ************************************************************************
//**********************************************************************************************
?>
<?php
// Creamos funcion galeria
function galeria( $post_id ) {
  // Luego creamos la variable $post, esta capta el post_id.
  $post = get_post($post_id);
  // Nos aseguramos que el post tenga galeria.
  if( ! has_shortcode( $post->post_content, 'gallery' ) )
  return;
  // Agregamos tamaño al shortcode, este esta en la pagina galeria (adm wp)
  add_filter( 'shortcode_atts_gallery', 'size_gallery' );
  // Recuperamos todas las galerias de este post.
  $galleries = get_post_galleries_images( $post );
  // Loop through all galleries found
  foreach( $galleries as $gallery ) {
    // Loop through each image in each gallery
    foreach( $gallery as $image ) {
      echo "<div class='col-sm-6 col-md-3'><div class='item link-gallery'><img class='img-responsive center-block img-gallery' src='$image'><p>Ver imagen</p></div></div>";
    }
  }
}

// Cambiamos los tamaños de imagenes subidas
add_theme_support( 'post-thumbnails' ); // soporte theme thumb
add_image_size( 'chico-thumb', 200, 111, true ); // tamaño y corte
add_image_size( 'medium-thumb', 600, 333, true ); // tamaño y corte
add_image_size( 'home-thumb', 900, 500, true ); // tamaño y corte (este usamos en el home $out)
// Creamos funcion para forzar tamaño de salida de las galerias "full size"
function size_gallery ( $out ) {
  remove_filter( current_filter(), __FUNCTION__ );
  $out['size'] = 'home-thumb';
  return $out;
}
?>
<?php
function stop_removing_tags(){
    remove_filter('the_content', 'wpautop');
}
add_action('init', 'stop_removing_tags'); ?>

<?php
//**********************************************************************************************
// **** META BOXES *****************************************************************************
//**********************************************************************************************
?>
<?php
/* agregamos metaboxes */
add_action( 'add_meta_boxes', 'msg_plano' );
function msg_plano() {
    add_meta_box( 'msg-plano-meta-box', __( '-----::::: Dont Forget :::::-----'), 'plano_meta_box_callback', 'page', 'normal', 'high' );
}

function plano_meta_box_callback() {
/* condicionamos el mensaje segun pagina. */
     global $post;
     if ( '16' == $post->ID ){
     echo "<div class='gif'> <img src='https://media.giphy.com/media/qQh0DBncuFJwQ/giphy.gif' alt='dont forget'> </div>";
     echo "<div class='mensaje-box'>Para el buen funcionamiento de esta sección, levanta cada imagen con un formato de 900x500. ;)</div>";
     }
     elseif ( '74' == $post->ID ){
     echo "<div class='gif'> <img src='https://media.giphy.com/media/s59Csd4R2DtQI/giphy.gif' alt='dont forget'> </div>";
     echo "<div class='mensaje-box'>Aquí es importante incorporar un keyword. Esto ayudará al seo del sitio. No superar los 36 caracteres. -> http://www.contarcaracteres.com/</div>";
     }
     else{
       echo "<div class='gif'> <img src='https://media.giphy.com/media/Hcw7rjsIsHcmk/giphy.gif' alt='dont forget'> </div>";
       echo "<div class='mensaje-box'>Antes de tipear o copiar, recuerda siempre pasar el texto por un notepad/bloc de notas, con esto borramos todo carácter o código extra que puede estropear nuestro mensaje. No agregar estilos, estos ya están en funcionamiento.</div>";
     }
}
?>

<?php
//**********************************************************************************************
// **** CSS EN ADMIN (METABOXES) ***************************************************************
//**********************************************************************************************
?>
<?php
function load_css() {
    wp_register_style( 'admin-style', get_template_directory_uri() . '/assets/css/admin-style.css', false, NULL, 'all' );
    wp_enqueue_style( 'admin-style' );
}

add_action( 'admin_enqueue_scripts', 'load_css' );
 ?>
